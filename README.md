# Install OpenSSH on Ubuntu

This repository contains instructions to install OpenSSH, an implementation of the Secure Schell protocol, which enables encryption when transferring data between machines (client-server). Accessing a server can occur seamlessly, without being asked for passwords.
