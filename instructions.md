* `sudo apt update; sudo apt install openssh-client; sudo apt install openssh-server`
* the relevant 'configuration' files are stored in `~/.ssh`; configurations can be changed in `~/.ssh/config`; in `~/.ssh/authorized_keys` are the keys recognized when connecting to the computer (that you are logged in) from remote computers; `~/.ssh/known_hosts` contains the addresses of the computers to connect to (see last point below)
* on the client machine
    * `ssh-keygen` (do not add a passphrase!); if `openssh` was already installed, check if there is already any key pair within `~/.ssh` (e.g., `id_rsa` and `id_rsa.pub`): this can be used more than once
    * copy the public key to the server: `ssh-copy-id -i ~/.ssh/id_rsa.pub technician@technician`
    * add in `~/.ssh/config` of your client machine (to specify which key goes with which server):
        ```
        Host technician
            AddKeysToAgent yes
            IdentityFile ~/.ssh/id_rsa
        ```
    * access typing `ssh technician@technician`
    * if one gets an error message such as `Warning: the ECDSA host key for 'technician' differs ...`, then type `ssh-keygen -R 192.168.1.123` (where the IP address is the one given in the warning message)
